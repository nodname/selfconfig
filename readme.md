```
(channel
  (name 'selfconfig)
  (url "https://bitbucket.org/nodname/selfconfig.git")
  (introduction
    (make-channel-introduction
      "b79660f80aa275cbe152666a2247291f32365ee0"
      (openpgp-fingerprint
        "9512 B1C7 FD0A 203D B091  E313 201D 3E4E 4ABB 672A"
      )
    )
  )
)
```
